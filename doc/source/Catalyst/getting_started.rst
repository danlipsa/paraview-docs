.. _ParaViewCatalystGettingStarted:

Getting Started
===============

This is a short introduction on how to use a C++ example_
from the ParaView code base. This code represents a simple simulation that
makes use of the Catalyst API.

.. tip::
  You can skip cloning the entrire ParaView source code by downloading just the example directory_.

Contents of the ``CxxFullExample`` directory:

* ``FEDriver.cxx``: the main loop of the simulation
* ``FEDataStructure.[h,cxx]``: simulation files describing the data structures of the driver.
* ``CatalystAdaptor.h``: the interface to the Catalyst library. Data
  descriptions and catalyst setup happens here. This is the main file to edit
  when you adapt the example for another simulation.

* ``catalyst_pipeline.py``: a script called at runtime
* ``catalyst_pipeline_live.py``: Alternative script to call at runtime that enables Live Visualization
* ``CMakeLists.txt``: The CMake code to configure and build the project. It demonstrates how to find the Catalyst library and link against it.

.. _example: https://gitlab.kitware.com/paraview/paraview/-/tree/master/Examples/Catalyst2/CxxFullExample
.. _directory: https://gitlab.kitware.com/paraview/paraview/-/archive/master/paraview-master.tar.gz?path=Examples/Catalyst2/CxxFullExample

Prerequisites
-------------

*  MPI
*  libcatalyst_ installed on your system.
*  CMake_
*  ParaView_ binary (version 5.10.1+)

.. _libcatalyst: https://catalyst-in-situ.readthedocs.io/en/latest
.. _CMake: https://cmake.org/download
.. _ParaView: https://www.paraview.org/download

Building the example
--------------------

::

  mkdir build
  cd build
  cmake -DCMAKE_PREFIX_PATH=<libcatalyst-install-dir> ../CxxFullExample
  cmake --build .


Simple Run
----------

Run the generated executable ``./bin/CxxFullExampleV2`` with ``catalyst_script.py`` as
a parameter.

The executable needs to link to the ParaViewCatalyst library at runtime.
To achieve this, we initialize ``CATALYST_IMPLEMENTATION_PATHS`` and ``CATALYST_IMPLEMENTATION_NAME``
to the path of the ParaViewCatalyst library and the name of the catalyst library implementation.
Finally, we pass ``catalyst_script.py`` as script argument:

::

   export CATALYST_IMPLEMENTATION_PATHS="<paraview-install-dir>/lib/catalyst"
   export CATALYST_IMPLEMENTATION_NAME=paraview
   ./bin/CxxFullExample catalyst_pipeline.py

Expected output:

::

  executing catalyst_pipeline
  -----------------------------------
  executing (cycle=0, time=0.0)
  bounds: (0.0, 69.0, 0.0, 64.9, 0.0, 55.9)
  velocity-magnitude-range: (0.0, 0.0)
  pressure-range: (1.0, 1.0)
  -----------------------------------
  executing (cycle=1, time=0.1)
  bounds: (0.0, 69.0, 0.0, 64.9, 0.0, 55.9)
  velocity-magnitude-range: (0.0, 6.490000000000001)
  pressure-range: (1.0, 1.0)
  -----------------------------------
  ...

.. note::

   Be careful that the MPI version used to build the simulation should be
   the same as the MPI used by ParaView. In case of errors, you may have
   to build ParaView by yourself, so ParaView and simulation share the same
   version of MPI. For this tutorial, you can also remove MPI-related lines
   in the example source code.


Generating Catalyst Scripts
---------------------------

To specify the pipelines executed each time Catalyst is triggered you can
modify the ``catalyst_script.py`` directly or generate one interactively through
the ParaView UI.

To generate a catalyst script, assemble your pipelines as usual and append
extractor(s) at the end of each pipeline.  Extractors can be created through the
:guilabel:`Extractors` menu and allow to extract and save data as meshes,
images or even tables. Once the pipeline is ready use :guilabel:`File > Save
Catalyst State` to export the state as a Python script which you can supply to
``./bin/CxxFullExampleV2``.  For more details on extractors see section
:ref:`sec:Extractors` in the UserGuide section.


Live Visualization
------------------

When saving a Catalyst State you have the option to enable **Live Visualization** in the export wizard

To create your own script, start creating a pipeline in ParaView. Use
:ref:`sec:Extractors`
to write your results (screenshots or meshes). Then use
:guilabel:`File > Save Catalyst State` to export the Python script. Live
Visualization is an option you can enable in the export wizard.


For an example try ``catalyst_pipeline_live.py`` to experiment with
taking a screenshot and running with live visualization
enabled. You can also load it as a State File to inspect its content.

Use :guilabel:`Catalyst > Connect` menu in ParaView so ParaView will wait for
Catalyst input. Then run your simulation as explained above. The
configured pipeline will appear in your ParaView session.

.. tip::
  Use :guilabel:`Catalyst > Pause Simulation` before starting the simulation to
  pause the simulation on the first timestep.  This allows inspection of the
  pipeline prior to running it. This is also useful for examples which iterate
  too fast to see the timestep updates.

.. note::
  When the simulation ends, it breaks the connection with the ParaView
  application so it is expected to have some error message like the
  following:

  ::

     ERROR: In <paraview_source_dir>/VTK/Parallel/Core/vtkSocketCommunicator.cxx, line 781
     vtkSocketCommunicator (0x563b4cb9b5b0): Could not receive tag. 1

     ERROR: In <paraview_source_dir>/Remoting/Core/vtkTCPNetworkAccessManager.cxx, line 296
     vtkTCPNetworkAccessManager (0x563b457d9000): Some error in socket processing.

Using ParaViewCatalyst in your simulation
-----------------------------------------

To enable the use of ParaViewCatalyst for your simulation code, look at the
``CatalystAdaptor.h`` file.  It wraps the main catalyst functions ``catalyst_*``
while providing the expected arguments. For the protocols used in each of the
calls see :ref:`here <ParaViewCatalystBlueprintProtocol>`.
