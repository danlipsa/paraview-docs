.. include:: ../../macros.hrst
.. include:: ../../abbreviations.hrst

.. _chapter:BeginningParaView:

Beginning: ParaView
###################

Introduction
============

This tutorial will describe how to start |paraview|, find information and
help about |paraview|, and describe some of the more important controls
used by ParaView.

The |ParaView| web site is located at https://www.paraview.org. New
versions of |paraview| can be found here, along with different tutorials
and advice. |ParaView| versions include 32 and 64 bit versions of Linux,
Windows and macOS. Training (practice) data is included in all ParaView
downloads, and can be accessed through **File → Open → Examples**.


.. admonition:: **Did you know?**
   :class: tip

   |paraview| now has an integrated help. It is found under the menu item **Help → Help**.

A simple example
================

Start ParaView
--------------

-  On Windows, go to **Start → All Programs → ParaView x.x.x →** and click **ParaView**.

-  On Mac, in the finder, go to the ParaView directory (in the Application directory where you
   installed ParaView) and click on **paraview**.

-  On Linux go into the |ParaView| directory (where you downloaded ParaView), then **bin** and type **paraview**.

Startup Screen
--------------

-  The **Startup Screen** includes two important links. Both of these
   links can also be found from the **Help** menu. They are the **Getting
   Started Guide** and **Example Visualizations**.  If desired click the
   **Don't show this window again**, and then click **Close**.

.. figure:: ../../images/Beginning_paraview_StartupScreen.jpg

Getting Started Guide
---------------------

-  The **Getting Started Guide** is a two page mini tutorial that
   shows fundamental |ParaView| usage.

.. figure:: ../../images/Beginning_paraview_GettingStartedGuide.jpg

-  **Example Visualizations** provide six finished visualizations.
   You can then play around with a |ParaView| pipeline.

.. figure:: ../../images/Beginning_paraview_ExampleVisualizations.jpg

Help Menu
---------

-  The **Help** menu looks like this.  Step through each menu option, then close the **Help** menu.

.. figure:: ../../images/HelpMenu.png

Open can.ex2
------------

-  Open can.ex2.

   -  can.ex2 is one of the datasets included with ParaView in the
      Examples folder.
   -  In |paraview|, **File → Open**.
   -  In the upper left corner, there is a folder called **Examples**. Go into this folder.
   -  Select can.ex2.
   -  **OK**.
   -  Under the **Properties** tab, all **Block Arrays** will be selected.
   -  **Apply**.

-  Turn off the node variables for VEL. Since any variable that is
   selected takes up memory, and since some datasets are huge, often the
   user will only read in the data that is needed for a run.

   -  Click **VEL**, turning the check box OFF.
   -  **Apply**.

-  The screen should now look like this. (The square will show up as
   red, since |ParaView| defaults to coloring by block, and the block we
   are seeing is red.) You are looking at the bottom of the plate that
   the can is sitting on.

.. figure:: ../../images/Beginning_paraview_2New.jpg
   :width: 1000px

-  Lets move the 3d object. Grab the can using the **left** mouse
   button. Try the **center** button. Try again with the **right**
   button. Try all three again holding down the **SHIFT** key. Try again
   holding down the X, Y and Z keys.
-  Place your mouse on a corner of the can. Now, hold the **CTRL** key down,
   and move the **RIGHT** mouse button up and down. You can zoom into and out of that
   location. Note that Macs use the **Command** key instead of the **CTRL** key.
-  The **Reset** icon is used to recenter your data.  Select it now.  To the right of this is the **Zoom to Box** icon, which looks like a magnifying glass.  Select it, and rubber band select the edge of the can.  **Reset**.  Try using the **SHFT** key and **Zoom to Box**.

.. figure:: ../../images/Beginning_paraview_Reset_Icon.png

-  Telling the **camera** to look up and down the **X**, **Y** and **Z** axes is done with the **+X**, **-X**, **+Y**, **-Y**, **+Z**, and **-Z** icons.

.. figure:: ../../images/Beginning_paraview_Axis_Icons.png

-  Now the screen looks like this:

.. figure:: ../../images/Beginning_paraview_2BNew.jpg
   :width: 1000px


-  To change the representation, change **Surface** to **Wireframe**
   (right below **Help**).

-  Then, change it to **Surface with Edges**.

-  Cycle through all of the other **representations**.  In order, these are:  **3D Glyphs**, **Feature Edges**, **Outline**, **Point Gaussian**, **Points**, **Slice**, **Surface**, **Surface LIC**, **Surface With Edges**, **Volume**, and **Wireframe**.  Note that we usually use the **Slice filter** for **slices**.

-  Finally, turn the **Representation** back to **Surface**.

-  Notice that the can dataset is being painted in two colors.  We are currently painting by **Block**.
-  Change the variable used for color. Change this from **Solid Color**.
   to Acceleration (Point **ACCL**). (This is found just below the
   **Sources** menu.) Everything should go blue.

.. figure:: ../../images/Beginning_paraview_3.jpg

-  Animate the can one frame forward using the **Next Frame** controls. Right above the window of the can are
   animation controls. Click the right arrow with a bar to its left
   once. The plate turns red.  Next to that is the **Play** button.  The leftmost control is **First Frame**.  It has
   a matching **Last Frame**.

.. figure:: ../../images/Beginning_paraview_4.png

.. admonition:: **Did you know?**
   :class: tip

   The can dataset has displacement information in
   it. We are actually running the plate into the can, and the whole
   object is moving.

-  Notice that our color map is not set correctly. It needs to be set over the whole range of
   displacement, so that it grades from blue to red.

.. admonition:: **Be careful!**
   :class: warning

   Very, very large data can take a long time to
   process. Don’t animate your data unless you have to with very
   large datasets.

-  Click the **Play** icon, running to the end of the simulation.
-  Click the **Rescale to Data Range** button.
-  **Play**.  **First Frame**.
-  Click the **Rescale to Custom Data Range** button.  Change the range to be 0.0 to 3.0e9.
-  **Play**.  **First Frame**.
-  Click the **Rescale to Data Range over All Timesteps** button.
-  **Play**.  **First Frame**.

Getting back GUI components
---------------------------

-  If you accidentally close the **Properties** tab, the **Information** tab
   or the **Pipeline Browser** tab, open them again from the **View** menu.
-  If you accidentally undock one of the tabs, just drag it back into
   place, wait for a gray shadow to appear, and drop it into place.

.. figure:: ../../images/Beginning_paraview_ViewB.png
