.. include:: macros.hrst

Welcome to ParaView Documentation !
===================================

This guide is split into several volumes:

-  **User's Guide**'s :numref:`chapter:Introduction` to :numref:`chapter:SavingResults` cover
   various aspects of data analysis and visualization with |ParaView|.

-  **Reference Manual**'s :numref:`chapter:PropertiesPanel` to :numref:`chapter:CustomizingParaView` provide
   details on various components in the UI and the scripting API.

- **Catalyst**: Instructions on how to use ParaView's implementation of the Catalyst API.

-  **Tutorials** are split into **Self-directed Tutorial** and **Classroom Tutorials**:

   -  **Self-directed Tutorial**'s :numref:`chapter:SelfDirectedTutorialIntroduction` to
      :numref:`chapter:FurtherReading` provide an introduction to the |ParaView| software and its history,
      and exercises on how to use |ParaView| that cover basic usage, batch Python scripting and visualizing large
      models.

   -  **Classroom Tutorials**'s :numref:`chapter:BeginningParaView` to :numref:`chapter:TargetedParaViewWeb` provide
      beginning, advanced, Python and batch, and targeted tutorial lessons on how to use |ParaView| that are presented
      as a 3-hour class internally within Sandia National Laboratories.

.. toctree::
   :maxdepth: 2
   :caption: ParaView User's Guide

   UsersGuide/index.rst

.. toctree::
   :maxdepth: 2
   :caption: ParaView Reference Manual

   ReferenceManual/index.rst

.. toctree::
   :maxdepth: 2
   :caption: Catalyst

   Catalyst/index.rst

.. toctree::
   :maxdepth: 2
   :caption: ParaView Tutorials

   Tutorials/index.rst

.. toctree::
   :maxdepth: 1
   :caption: Appendix

   references

Documentation Source
--------------------

This documentation is generated from source files in the
`ParaView Documentation <https://gitlab.kitware.com/paraview/paraview-docs>`_ project.
